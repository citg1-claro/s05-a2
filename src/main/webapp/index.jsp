<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Job Finder App</title>
</head>
<body>

<h1>Welcome to Job Finder</h1>
    <form action="RegisterServlet" method="post">
        <label for="firstName">First Name:</label>
        <input type="text" id="firstName" name="firstName" required><br><br>
        
        <label for="lastName">Last Name:</label>
        <input type="text" id="lastName" name="lastName" required><br><br>
        
        <label for="phone">Phone Number:</label>
        <input type="tel" id="phone" name="phone" required><br><br>
        
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br><br>
        
        <label>How did you discover the app?</label><br>
        <input type="radio" id="friends" name="appDiscovery" value="Friends" required>
        <label for="friends">Friends</label><br>
        <input type="radio" id="socialMedia" name="appDiscovery" value="Social Media">
        <label for="socialMedia">Social Media</label><br>
        <input type="radio" id="others" name="appDiscovery" value="Others">
        <label for="others">Others</label><br><br>
        
        <label>Are you an employer or applicant?</label><br>
		<input type="radio" id="employer" name="userType" value="employer" required>
		<label for="employer">Employer</label><br>
		<input type="radio" id="applicant" name="userType" value="applicant">
		<label for="applicant">Applicant</label><br><br>
		
        <label for="dob">Date of Birth:</label>
        <input type="date" id="dob" name="dob" required><br><br>
        
        <label for="profileDescription">Profile Description:</label><br>
        <textarea id="profileDescription" name="profileDescription" rows="5" cols="30" required></textarea><br><br>
        
        <input type="submit" value="Register">  <input type="reset">
    </form>


</body>
</html>