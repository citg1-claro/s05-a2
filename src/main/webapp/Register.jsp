<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
    <title>Registration Confirmation</title>

</head>
<body>
 <h1>Registration Confirmation</h1>
    <p>Thank you for registering with Job Finder. Your information has been saved.</p>
    
    <p>First Name: <%= session.getAttribute("firstName") %></p>
    <p>Last Name: <%= session.getAttribute("lastName") %></p>
    <p>Phone: <%= session.getAttribute("phone") %></p>
    <p>Email: <%= session.getAttribute("email") %></p>
    <p>App Discovery: <%= session.getAttribute("appDiscovery") %></p>
    <p>Date of Birth: <%= session.getAttribute("dob") %></p>
    <p>User Type: <%= session.getAttribute("userType") %></p>
    <p>Profile Description: <%= session.getAttribute("profileDescription") %></p>
    <form action="home.jsp" method="get">
    <input type="submit" value="Submit">
	</form>
    
	<input type="button" value="Back" onclick="history.back()">
</body>
</html>