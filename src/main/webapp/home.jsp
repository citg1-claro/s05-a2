<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Job Finder Home</title>
</head>
<body>
<h1>Welcome to Job Finder</h1>
    
    <%
        // Retrieve user's full name from HttpSession
        String fullName = (String) session.getAttribute("firstName");
        
        // Check user type and display appropriate message
        String userType = (String) session.getAttribute("userType");
        if (userType.equals("employer")) {
    %>
        <p>Hello, <%= fullName %>. You are logged in as an employer.</p>
        <p>You may now start browsing applicant profiles.</p>
    <%
        } else {
    %>
        <p>Hello, <%= fullName %>. You are logged in as an applicant.</p>
        <p>You may now start looking for your career opportunity.</p>
    <%
        }
    %>
    
    <p><a href="logout.jsp">Logout</a></p>
</body>
</html>