package com.zuitt;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;


public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Retrieve first and last name from login form
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        
        // Concatenate first and last name into full name
        String fullName = firstName + " " + lastName;
        
        // Store full name in HttpSession
        HttpSession session = request.getSession();
        session.setAttribute("fullName", fullName);
        
        // Redirect user to home page
        response.sendRedirect("home.jsp");
	}

}
