package com.zuitt;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;



public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public RegisterServlet() {
        super();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String appDiscovery = request.getParameter("appDiscovery");
        String dob = request.getParameter("dob");
        String userType = request.getParameter("userType");
        String profileDescription = request.getParameter("profileDescription");
        
        HttpSession session = request.getSession();
        session.setAttribute("firstName", firstName);
        session.setAttribute("lastName", lastName);
        session.setAttribute("phone", phone);
        session.setAttribute("email", email);
        session.setAttribute("appDiscovery", appDiscovery);
        session.setAttribute("dob", dob);
        session.setAttribute("userType", userType);
        session.setAttribute("profileDescription", profileDescription);
        
        request.getRequestDispatcher("Register.jsp").forward(request, response);
	}

}
